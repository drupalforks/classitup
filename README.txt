Class it up provides classes based on information Drupal knows about.

* Adds region machine names as classes on blocks.
* Adds a class on any page with one individual piece of content.

### Related modules:

* https://www.drupal.org/project/twigsuggest
* https://www.drupal.org/project/body_node_id_class

See the project homepage: https://www.drupal.org/project/classitup
